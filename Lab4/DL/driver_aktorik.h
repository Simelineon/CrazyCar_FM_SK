#ifndef __DRIVER_AKTORIK
#define __DRIVER_AKTORIK

#include <msp430.h>
#include "../HAL/hal_timerA1.h"

#define MAX_RPW 50 	//in 10�s
#define MIN_RPW 100
#define MIN_FPW 150
#define MAX_FPW 200

#define ESC_CONFIRM 125	//Zwischen den beiden MIN Werten



void Driver_SetSteering(signed char);
void Driver_SteeringInit();

void Driver_SetThrottle(signed char);
void Driver_ThrottleInit();

#endif
