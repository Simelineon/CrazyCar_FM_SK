#include <msp430.h>
#include "HAL/hal_general.h"
#include "DL/driver_general.h"

/*
 * main.c
 */

extern ButtonCom button;

void main(void) {

	HAL_Init();
    Driver_Init();

    while (1) {
    	if (button.active) {							//wenn button gedr�ckt wurde
    		if (button.button == STRUCT_BTN_START) {	//wenn START Button gedr�ckt wurde
    			SET_ON(P8OUT,LCD_BL);					//Display Hintergrundbeleuchtung anschalten
    		    Driver_SetThrottle(70);
    		    Driver_SetSteering(50);
    		} else {									//sonst
    			SET_OFF(P8OUT,LCD_BL);					//Display Hintergrundbeleuchtung ausschalten
    		    Driver_SetThrottle(0);
    		    Driver_SetSteering(0);
    		}
    		button.active = FALSE;						//Button wieder auf NOT Active setzen (da flag gehandled wurde)
    	}
    }
}
