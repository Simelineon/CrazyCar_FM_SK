#ifndef __HAL_TIMER_A1
#define __HAL_TIMER_A1

#include <msp430.h>
#include "hal_gpio.h"

#define TACCR0_WERT 41667

#define STEERING_WERT_LEFT 2750
#define STEERING_WERT_CENTER 3750
#define STEERING_WERT_RIGHT 4750

void HAL_TimerA1_Init();

#endif
