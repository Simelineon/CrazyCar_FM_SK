#include <hal_timerB0.h>

extern unsigned long long rpm_sensor_interrupts;
extern short speeed;

long long save = 0, modulo_cnt = 0;

void HAL_TimerB0_Init() {
	TBCTL |= TBSSEL__SMCLK + MC__UP + ID__8;	//SMCLK verwenden, UPcount Modus verwenden, Divider auf "/4" setzen
	TB0EX0 = TBIDEX__8;
	TBCCTL0 = CCIE + OUTMOD_3;					//Enable Interrupts and set Outmod to Input/Output
	TBCTL |= TBCLR;								//Timer_B clear
	TBCCR0 = 0x144;								//definieren dass bis zu dem Wert 0x144 gez�hlt werden soll
	//120Hz Takt
}

//Timer B0 interrupt service routine
#pragma vector=TIMERB0_VECTOR
__interrupt void TIMERB0 (void) {
	if (++modulo_cnt >= 30) {				//alle 120/15 Hz = 8 Hz
		modulo_cnt = 0;
		if (save != 0) {
			speeed = (rpm_sensor_interrupts - save);
			if (speeed < 0) speeed = 0;
			speeed = speeed >> 3;
		}
		save = rpm_sensor_interrupts;
	}
	ADC12CTL0 |= ADC12SC; //Triggern des ADC's
}
