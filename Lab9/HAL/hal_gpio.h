#ifndef __HAL_GPIO_H
#define __HAL_GPIO_H 1

#include <msp430.h>


//======================= GENERAL DEFINITIONS =======================
//OTHER DEFINITIONS
#define TRUE 	1
#define FALSE 	0
#define BYTE	8
//======================= GENERAL DEFINITIONS =======================


//======================= MACROS =======================
//PIN DIRECTION MACROS
#define SET_INPUT(p_dir,bit)							(p_dir &= ~bit)
#define SET_OUTPUT(p_dir,bit)							(p_dir |= bit)
#define SET_ALL_OUTPUT(p_dir)							(p_dir |= 0xFF)
#define SET_PULLUP(p_ren,p_out,bit)						({p_ren |= bit;p_out|=bit;})

//PIN OUTPUT MACROS
#define SET_HIGH(p_out,bit)								(p_out|=bit)
#define SET_LOW(p_out,bit)								(p_out&=~bit)
#define TOGGLE(p_out,bit)								(p_out^=bit)

//ALIAS
#define SET_ON(p_out,bit)								SET_HIGH(p_out,bit)
#define SET_OFF(p_out,bit)								SET_LOW(p_out,bit)

//PIN INTERRUPT MACROS
#define INTERRUPT_ENABLE(p_ie,bit)						(p_ie|=bit)
#define INTERRUPT_DISABLE(p_ie,bit)						(p_ie&=~bit)
#define INTERRUPT_EDGE_SELECT_FALLING(p_ies,bit)		(p_ies|=bit)
#define INTERRUPT_EDGE_SELECT_RISING(p_ies,bit)			(p_ies&=~bit)
#define INTERRUPT_RESET_FLAG(p_ifg,bit)					(p_ifg&=~bit)
//======================= MACROS =======================


//======================= PIN DEFINITIONS =======================
//PORT 1
#define RPM_SENSOR BIT3
#define RPM_SENSOR_DIR BIT4
#define I2C_INT_MOTION BIT5
#define START_BUTTON BIT6
#define STOP_BUTTON BIT7

//PORT 2
#define DEBUG_TXD BIT0
#define DEBUG_RXD BIT1
#define AUX_PIN_1 BIT2
#define AUX_PIN_2 BIT3
#define AUX_PIN_3 BIT4
#define AUX_PIN_4 BIT5
#define I2C_SDA_MOTION BIT6
#define I2C_SCL_MOTION BIT7

//PORT 3
#define THROTTLE BIT2
#define STEERING BIT3
#define SMCLK BIT4
#define DISTANCE_FRONT_EN BIT7

//PORT 4
#define LINE_FOLLOW_2 BIT3
#define LINE_FOLLOW_1 BIT4
#define LINE_FOLLOW_3 BIT5
#define LINE_FOLLOW_4 BIT6
#define LINE_FOLLOW_5 BIT7

//PORT 5

//PORT 6
#define DISTANCE_RIGHT BIT0
#define DISTANCE_LEFT BIT1
#define DISTANCE_FRONT BIT2
#define VBAT_MEASURE BIT3
#define DISTANCE_LEFT_EN BIT4

//PORT 7
#define XT2IN BIT2
#define XT2OUT BIT3

//PORT 8
#define LCD_BL BIT0
#define LCD_SPI_CS BIT1
#define UART_TXD_AUX BIT2
#define UART_RXD_AUX BIT3
#define LCD_SPI_CLK BIT4
#define LCD_SPI_MOSI BIT5
#define LCD_SPI_MISO BIT6
#define LCD_DATACMD BIT7

//PORT 9
#define LCD_RESET BIT0
#define DISTANCE_RIGHT_EN BIT7
//======================= PIN DEFINITIONS =======================


//======================= BUTTON STRUCTURE =======================
#define STRUCT_BTN_START	1
#define STRUCT_BTN_STOP		0
typedef struct {
	unsigned char active; // TRUE 1 / FALSE 0
	unsigned char button; // Button number
} ButtonCom;
//======================= BUTTON STRUCTURE =======================


//======================= GPIO INITIALIZATION =======================
void HAL_GPIO_Init();
//======================= GPIO INITIALIZATION =======================

#endif
