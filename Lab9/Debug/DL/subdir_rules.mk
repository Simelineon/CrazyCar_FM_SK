################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
DL/driver_aktorik.obj: ../DL/driver_aktorik.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/CCS/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=restricted -Ooff --opt_for_speed=0 --use_hw_mpy=F5 --include_path="C:/CCS/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab9/AL" --include_path="C:/Users/felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab9/DL" --include_path="C:/Users/felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab9/HAL" --include_path="C:/CCS/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power=all -g --define=__MSP430F5335__ --display_error_number --diag_warning=225 --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="DL/driver_aktorik.d_raw" --obj_directory="DL" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

DL/driver_general.obj: ../DL/driver_general.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/CCS/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=restricted -Ooff --opt_for_speed=0 --use_hw_mpy=F5 --include_path="C:/CCS/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab9/AL" --include_path="C:/Users/felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab9/DL" --include_path="C:/Users/felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab9/HAL" --include_path="C:/CCS/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power=all -g --define=__MSP430F5335__ --display_error_number --diag_warning=225 --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="DL/driver_general.d_raw" --obj_directory="DL" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

DL/driver_lcd.obj: ../DL/driver_lcd.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/CCS/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=restricted -Ooff --opt_for_speed=0 --use_hw_mpy=F5 --include_path="C:/CCS/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab9/AL" --include_path="C:/Users/felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab9/DL" --include_path="C:/Users/felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab9/HAL" --include_path="C:/CCS/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power=all -g --define=__MSP430F5335__ --display_error_number --diag_warning=225 --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="DL/driver_lcd.d_raw" --obj_directory="DL" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

DL/driver_sensor_map.obj: ../DL/driver_sensor_map.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/CCS/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=restricted -Ooff --opt_for_speed=0 --use_hw_mpy=F5 --include_path="C:/CCS/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab9/AL" --include_path="C:/Users/felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab9/DL" --include_path="C:/Users/felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab9/HAL" --include_path="C:/CCS/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power=all -g --define=__MSP430F5335__ --display_error_number --diag_warning=225 --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="DL/driver_sensor_map.d_raw" --obj_directory="DL" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


