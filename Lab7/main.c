#include <msp430.h>
#include "HAL/hal_general.h"
#include "DL/driver_general.h"
#include "string.h"

#define SYMBOL_HEART	3


/*
 * main.c
 */

extern ButtonCom button;
extern USCIB1_SPICom LCD_Data;
extern ADC12Com ADC_Data;

long rpm_sensor_interrupts = 0;

void main(void) {
	HAL_Init();
    Driver_Init();

    int weg = 0; //weg in cm

    while (1) {
    	if (ADC_Data.Status.B.ADCrdy && LCD_Data.Status.B.TxSuc == 1) {
			Driver_LCD_WriteText("Front:", strlen("Front:"), 0, 0);
			Driver_LCD_WriteValue(ADC_Data.SensorFront, "   ", 0, 6*6);
			Driver_LCD_WriteText("Left :", strlen("Left :"), 1, 0);
			Driver_LCD_WriteValue(ADC_Data.SensorLeft, "   ", 1, 6*6);
			Driver_LCD_WriteText("Right:", strlen("Right :"), 2, 0);
			Driver_LCD_WriteValue(ADC_Data.SensorRight, "   ", 2, 6*6);
			Driver_LCD_WriteText("Batt :", strlen("Batt :"), 3, 0);
			Driver_LCD_WriteValue(ADC_Data.VBat, "   ", 3, 6*6);
			ADC_Data.Status.B.ADCrdy = 0;
    	}

    	if (button.active) {							//wenn button gedr�ckt wurde
    		if (button.button == STRUCT_BTN_START) {	//wenn START Button gedr�ckt wurde
    		    //Driver_SetThrottle(25);
    		    //Driver_SetSteering(0);
    		} else {									//sonst
    		    //Driver_SetThrottle(0);
    		    //Driver_SetSteering(0);
    		    weg = (11 * rpm_sensor_interrupts)/12;	//weg berechnen

    			Driver_LCD_Clear();
    			char * text = "Weg:";
    			Driver_LCD_WriteText(text, strlen(text), 0, 0);
    			Driver_LCD_WriteValue(weg, "cm", 0, 4);
    		}
    		button.active = FALSE;						//Button wieder auf NOT Active setzen (da flag gehandled wurde)
    	}
    }
}
