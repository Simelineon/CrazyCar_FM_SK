#ifndef __HAL_WDT_INIT_H
#define __HAL_WDT_INIT_H 1

#include <msp430.h>

void HAL_Wdt_Init();

#endif
