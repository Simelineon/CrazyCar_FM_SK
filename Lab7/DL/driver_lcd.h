#ifndef __DRIVER_LCD
#define __DRIVER_LCD

#include <msp430.h>
#include "../HAL/hal_gpio.h"
#include "hal_usciB1.h"

//MACROS
#define LCD_COMMAND 	SET_LOW(P8OUT,LCD_DATACMD);
#define LCD_DATA 		SET_HIGH(P8OUT,LCD_DATACMD);
#define WAIT_FOR_LCD()	while(LCD_Data.Status.B.TxSuc==0);

//DEFINES
#define C_LCD_RESET			0xE2
#define C_LCD_BIAS			0xA3
#define C_ADC_SEL_NORMAL	0xA0
#define C_COMMON_REVERSE	0xC8
#define C_RES_RATIO			0x24
#define C_ELEC_VOL_MODE		0x81
#define C_ELEC_VOL_VALUE	0x0F	//Kontrast Einstellung
#define C_POWER_CONT		0x2F
#define C_DISPLAY_ON		0xAF

#define LCD_MAX_COLM		128
#define FIRST_PAGE 			0xB0 //0xB0 f�r erste Page , 0xB1 f�r zweite Page
#define LAST_PAGE 			0xB7

#define DELAY_TIME			500000
#define INIT_COMMAND_CNT	9

#define MSB_COL_ADDR 0x10 //0x10, 0x11 ,..,0x1F f�r spalten �ber 16
#define LSB_COL_ADDR 0x00 //0x01, 0x02 ,..,0x0F f�r spalten bis 15



void Driver_LCD_Init();
void Driver_LCD_WriteCommand(unsigned char *data, unsigned char len);
void Driver_LCD_WriteText(char *text , unsigned char text_length , unsigned char page , unsigned char col);
void Driver_LCD_WriteSymbol(unsigned char symbol , unsigned char page , unsigned char col);
void Driver_LCD_WriteValue(short value, char *unit, unsigned char page, unsigned char col);
void Driver_LCD_Clear();
void Driver_LCD_SetPosition(unsigned char , unsigned char);

#endif
