#include <stdlib.h>
#include "driver_lcd.h"
#include "driver_fonttable.h"

extern USCIB1_SPICom LCD_Data;

unsigned char LCD_Pos_Array[] = {FIRST_PAGE,MSB_COL_ADDR,LSB_COL_ADDR};

void Driver_LCD_WriteText(char *text , unsigned char text_length , unsigned char page , unsigned char col) {
	WAIT_FOR_LCD();
    unsigned char i, text_length_cnt, col_pos = col, minus_chars = 0;
    Driver_LCD_SetPosition(page, col);	// Display Cursor setzen
    for(text_length_cnt=0; text_length_cnt < text_length; text_length_cnt++) { //Textl�nge des Strings abarbeiten

    	if (col + text_length_cnt*fonts_width_max - minus_chars*fonts_width_max >= LCD_MAX_COLM-fonts_width_max-1) {
    		minus_chars = text_length_cnt;
    		++page;		//N�chste Page
    		col = 0;	//Erste Column
    		col_pos = 0;
    	    Driver_LCD_SetPosition(page, col);	// Display Cursor setzen
    	}

    	for(i=0; i < fonts_width_max; i++) { //Character darstellen
            LCD_Data.TxData.Data[i] = font_table[*text][i];
            col_pos++;
        }
        LCD_Data.TxData.len = fonts_width_max;
        LCD_DATA;
        HAL_USCIB1_Transmit();
        WAIT_FOR_LCD();
        text++;
    }
    Driver_LCD_SetPosition(page, col_pos);	// Display Cursor setzen
}

void Driver_LCD_WriteValue(short value, char *unit, unsigned char page , unsigned char col) {
	WAIT_FOR_LCD();
	unsigned char i, cur_pos = 0, col_pos = col;
	unsigned char zahl_arr[7] = {};
	Driver_LCD_SetPosition(page, col);	// Display Cursor setzen
	while(value >= 0) { //Textl�nge der Zahl
		zahl_arr[cur_pos++] = value % 10;
		value = value / 10;
		if (value == 0) value = -1;
	}

	for (;cur_pos > 0; --cur_pos) {
		for(i=0; i < fonts_width_max; i++) { //Character darstellen
			LCD_Data.TxData.Data[i] = font_table[48+zahl_arr[cur_pos-1]][i];
			col_pos++;
		}
		LCD_Data.TxData.len = fonts_width_max;
		LCD_DATA;
		HAL_USCIB1_Transmit();
		WAIT_FOR_LCD();
	}

	if (unit != 0) Driver_LCD_WriteText(unit, strlen(unit), page, col_pos);
}

void Driver_LCD_WriteSymbol(unsigned char symbol , unsigned char page , unsigned char col) {
    WAIT_FOR_LCD();
    unsigned char i, text_length_cnt, col_pos = col, minus_chars = 0;
    Driver_LCD_SetPosition(page, col);	// Display Cursor setzen
    if (col + fonts_width_max >= LCD_MAX_COLM-fonts_width_max-1) {
		++page;		//N�chste Page
		col = 0;	//Erste Column
		Driver_LCD_SetPosition(page, col);	// Display Cursor setzen
	}

	for(i=0; i < fonts_width_max; i++) { //Character darstellen
		LCD_Data.TxData.Data[i] = symbols_table[symbol][i];
		col_pos++;
	}
	LCD_Data.TxData.len = fonts_width_max;
	LCD_DATA;
	HAL_USCIB1_Transmit();
	WAIT_FOR_LCD();
}

void Driver_LCD_SetPosition(unsigned char page_sel , unsigned char col_sel) {
    unsigned char msb_col = (col_sel >> 4) & 0x0F; //0001 xxxx shiften zu MSB Wert
    unsigned char lsb_col = col_sel & 0x0F; //0000 xxxx maskieren auf LSB Wert

    LCD_Pos_Array[0] = FIRST_PAGE | page_sel; 	//Page-Wert muss als erstes gesetzt werden
    LCD_Pos_Array[1] = MSB_COL_ADDR | msb_col; 	//MSB Column Address
    LCD_Pos_Array[2] = LSB_COL_ADDR | lsb_col; 				//LSB Column Address

	//LCD_POS_Array Befehle an LCD senden
	Driver_LCD_WriteCommand(LCD_Pos_Array, 3);
    WAIT_FOR_LCD();
}

void Driver_LCD_WriteCommand(unsigned char *data , unsigned char data_length) {
	WAIT_FOR_LCD();
	LCD_COMMAND;
	unsigned char i;
    for(i=0; i < data_length; i++){
        LCD_Data.TxData.Data[i] = data[i];
    }
    LCD_Data.TxData.len = data_length;
    LCD_Data.TxData.cnt = 0;
    HAL_USCIB1_Transmit();
}

void Driver_LCD_Clear() {
	WAIT_FOR_LCD();
	unsigned char i, j;
	LCD_Pos_Array[1] = MSB_COL_ADDR;
	LCD_Pos_Array[2] = LSB_COL_ADDR;
	for(j=0x00; (FIRST_PAGE+j) <= LAST_PAGE; j++) {
		LCD_Pos_Array[0] = FIRST_PAGE+j;
		Driver_LCD_WriteCommand(LCD_Pos_Array , 3);
		WAIT_FOR_LCD();

		LCD_DATA;
		for(i=0; i< LCD_MAX_COLM; i++) {
			LCD_Data.TxData.Data[i] = 0x00;
		}

		LCD_Data.TxData.len = LCD_MAX_COLM;
		HAL_USCIB1_Transmit();
	}
}


void Driver_LCD_Init() {
	SET_ON(P8OUT,LCD_BL);					//Display Hintergrundbeleuchtung anschalten
    SET_LOW(P9OUT,LCD_RESET); 				//Reset Line LOW
    __delay_cycles (DELAY_TIME); 			//Wait for Display
    SET_HIGH(P9OUT,LCD_RESET); 				//Reset Line HIGH

    unsigned char LCD_Init_Array[] = {
    		C_LCD_RESET,
			C_LCD_BIAS,
			C_ADC_SEL_NORMAL,
			C_COMMON_REVERSE,
			C_RES_RATIO,
			C_ELEC_VOL_MODE,
			C_ELEC_VOL_VALUE,
			C_POWER_CONT,
			C_DISPLAY_ON
    };

    Driver_LCD_WriteCommand(LCD_Init_Array, INIT_COMMAND_CNT);
    WAIT_FOR_LCD();

    //Testen..
    //unsigned char all_black = 0xA5;
    //Driver_LCD_WriteCommand(&all_black,1);
    //WAIT_FOR_LCD();

    Driver_LCD_Clear(); //LCD alle pixel l�schen
    WAIT_FOR_LCD();
}
