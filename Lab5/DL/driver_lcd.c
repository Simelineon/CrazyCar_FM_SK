#include <stdlib.h>
#include "driver_lcd.h"
#include "driver_fonttable.h"

extern USCIB1_SPICom LCD_Data;

unsigned char LCD_Pos_Array[] = {0,0,0};

void Driver_LCD_WriteText(char *text , unsigned char text_length , unsigned char page , unsigned char col) {
    unsigned char i, text_length_cnt, col_pos = col;

    Driver_LCD_SetPosition(page, col);	// Display Cursor setzen

    for(text_length_cnt=0;text_length_cnt < text_length; text_length_cnt++) { //Textl�nge des Strings abarbeiten
        for(i=0; i < fonts_width_max; i++) { //Jeden Character des Strings
            LCD_Data.TxData.Data[i] = font_table[*text][i];
            col_pos++;
        }

        LCD_DATA;
        HAL_USCIB1_Transmit();
        WAIT_FOR_LCD();

        //TODO Column �overflow�?

        text++;
    }
}

void Driver_LCD_WriteCommand(unsigned char *data , unsigned char data_length) {
	LCD_COMMAND;

	unsigned char i;
    WAIT_FOR_LCD(); //warten auf senden
    for(i=0; i < data_length; i++){
        LCD_Data.TxData.Data[i] = data[i];
    }
    LCD_Data.TxData.len = data_length;
    LCD_Data.TxData.cnt = 0;
    HAL_USCIB1_Transmit();
}

void Driver_LCD_Clear() {
	unsigned char i, j;
	for(j=0x00; (FIRST_PAGE+j) <= LAST_PAGE; ++j) {
		LCD_Pos_Array[0] = FIRST_PAGE+j;
		Driver_LCD_WriteCommand(LCD_Pos_Array , 3);
		WAIT_FOR_LCD(); //warten auf senden

		for(i=0; i < LCD_MAX_COLM; ++i) {
			LCD_Data.TxData.Data[i] = 0x00;
		}

		LCD_DATA;
		LCD_Data.TxData.len = LCD_MAX_COLM;
		LCD_Data.TxData.cnt = 0;
		HAL_USCIB1_Transmit();
		WAIT_FOR_LCD(); //warten auf senden
	}
}


void Driver_LCD_SetPosition(unsigned char page_sel , unsigned char col_sel) {
    unsigned char msb_col = (col_sel >> 4); //0001 xxxx shiften zu MSB Wert
    unsigned char lsb_col = col_sel & 0x0F; //0000 xxxx maskieren auf LSB Wert

    LCD_Pos_Array[0] = page_sel; 	//Page-Wert muss als erstes gesetzt werden
    LCD_Pos_Array[1] = msb_col; 	//MSB Column Address
    LCD_Pos_Array[2] = lsb_col; 	//LSB Column Address

	//LCD_POS_Array Befehle an LCD senden
	Driver_LCD_WriteCommand(LCD_Pos_Array, 3);
    WAIT_FOR_LCD();
}


void Driver_LCD_Init() {
	SET_ON(P8OUT,LCD_BL);					//Display Hintergrundbeleuchtung anschalten
    SET_LOW(P9OUT,LCD_RESET); 				//Reset Line LOW
    __delay_cycles (DELAY_TIME); 			//Wait for Display
    SET_HIGH(P9OUT,LCD_RESET); 				//Reset Line HIGH

    unsigned char LCD_Init_Array[] = {
    		C_LCD_RESET,
			C_LCD_BIAS,
			C_ADC_SEL_NORMAL,
			C_COMMON_REVERSE,
			C_RES_RATIO,
			C_ELEC_VOL_MODE,
			C_ELEC_VOL_VALUE,
			C_POWER_CONT,
			C_DISPLAY_ON
    };

    Driver_LCD_WriteCommand(LCD_Init_Array, INIT_COMMAND_CNT);
    WAIT_FOR_LCD();

    //Testen..
    //unsigned char all_black = 0xA5;
    //Driver_LCD_WriteCommand(&all_black,1);

    Driver_LCD_Clear(); //LCD alle pixel l�schen
    WAIT_FOR_LCD();
}
